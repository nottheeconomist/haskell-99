-- 11
-- from #9 in one.hs
pack :: Eq a => [a] -> [[a]]
pack [] = []
pack [x] = [[x]]
pack xss@(x : xs) =
  let [subgroup, rest] = takeIf [] (== x) xss
   in subgroup : pack rest
  where
    takeIf :: [a] -> (a -> Bool) -> [a] -> [[a]]
    takeIf acc _ [] = [acc, []]
    takeIf acc f xss@(x : xs)
      | f x = takeIf (x : acc) f xs
      | otherwise = [acc, xss]

data RunLengthEncode a = Single a | Multiple Int a deriving (Show)

encodeModified :: Eq a => [a] -> [RunLengthEncode a]
encodeModified = go . pack
  where
    go [] = []
    go (x : xs)
      | length x == 1 = Single (head x) : go xs
      | otherwise = Multiple (length x) (head x) : go xs

-- 12
decodeModified :: [RunLengthEncode a] -> [a]
decodeModified [] = []
decodeModified (Single x : xs) = x : decodeModified xs
decodeModified (Multiple 0 x : xs) = decodeModified xs
decodeModified (Multiple n x : xs) = x : decodeModified (Multiple (n -1) x : xs)

-- 13
encodeDirect :: Eq a => [a] -> [RunLengthEncode a]
encodeDirect [] = []
encodeDirect (x : xs) = go 1 x xs
  where
    go n k []
      | n == 1 = [Single k]
      | otherwise = [Multiple n k]
    go n k xss@(x : xs)
      | x == k = go (n + 1) x xs
      | x /= k && n == 1 = Single k : encodeDirect xss
      | x /= k && n > 1 = Multiple n k : encodeDirect xss

-- 14
dupli :: [a] -> [a]
dupli [] = []
dupli (x : xs) = x : x : dupli xs

-- 15
repli :: [a] -> Int -> [a]
repli [] _ = []
repli (x : xs) 0 = []
repli (x : xs) n = multiple n x ++ repli xs n
  where
    multiple 0 _ = []
    multiple n x = x : multiple (n -1) x

-- 16
dropEvery :: [a] -> Int -> [a]
dropEvery [] _ = []
dropEvery xss n = go n xss
  where
    go _ [] = []
    go 1 (_ : xs) = dropEvery xs n
    go i (x : xs) = x : go (i -1) xs

-- 17
split :: [a] -> Int -> ([a], [a])
split [] _ = ([], [])
split xss n = go [] xss n
  where
    go acc xss 0 = (reverse acc, xss)
    go acc (x : xs) n = go (x : acc) xs (n -1)

-- 18
slice :: [a] -> Int -> Int -> [a]
slice [] _ _ = []
slice xss 1 0 = []
slice (x : xs) 1 k = x : slice xs 1 (k -1)
slice (x : xs) i k = slice xs (i -1) (k -1)

-- 19
rotate :: [a] -> Int -> [a]
rotate [] _ = []
rotate xs 0 = xs
rotate xs n
  | n > 0 = lastHalf ++ firstHalf
  | n < 0 = rotate xs (length xs + n)
  where
    firstHalf = take n xs
    lastHalf = drop n xs

-- 20
removeAt :: Int -> [a] -> (a, [a])
removeAt _ [] = (undefined, [])
removeAt n xss = go [] n xss
  where
    go acc 1 (x:xs) = (x, acc ++ xs)
    go acc n (x:xs) = go (acc ++ [x]) (n-1) xs
