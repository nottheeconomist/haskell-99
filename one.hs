-- 1
myLast :: [a] -> a
myLast [x] = x
myLast (x:xs) = myLast xs

-- 2
myButLast :: [a] -> a
myButLast [x, _] = x
myButLast (_:xs) = myButLast xs

-- 3
elementAt :: [a] -> Int -> a
elementAt (x:_) 1 = x
elementAt (_:xs) n = elementAt xs (n-1)

-- 4
myLength :: [a] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

-- 5
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

-- 6
isPalindrome :: Eq a => [a] -> Bool
isPalindrome [] = True
isPalindrome [_] = True
isPalindrome xs = xs == myReverse xs

-- 7
data NestedList a = Elem a | List [NestedList a]
flatten :: NestedList a -> [a]
flatten (Elem x) = [x]
flatten (List []) = []
flatten (List (x:xs)) = flatten x ++ flatten (List xs)

-- 8
compress :: Eq a => [a] -> [a]
compress [] = []
compress (x:xs) = x : compress' x xs
    where compress' :: Eq a => a -> [a] -> [a]
          compress' _ [] = []
          compress' last (x:xs)
            | x == last = compress' x xs
            | otherwise = x : compress' x xs

-- 9
pack :: Eq a => [a] -> [[a]]
pack [] = []
pack [x] = [[x]]
pack xss@(x:xs) = let [subgroup, rest] = takeIf [] (==x) xss
                  in subgroup : pack rest
    where takeIf :: [a] -> (a -> Bool) -> [a] -> [[a]]
          takeIf acc _ [] = [acc, []]
          takeIf acc f xss@(x:xs)
            | f x = takeIf (x:acc) f xs
            | otherwise = [acc, xss]

-- 10
encode :: Eq a => [a] -> [(Int, a)]
encode xs = [(length el, head el) | el <- pack xs]
